import java.sql.*;


public class Baza {

    Connection conn;

    public Baza (){
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:slownie.db");

        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public String pobierzLiczbeSlownie (Jezyk jezyk, int liczba){
        String sql = "SELECT * FROM slownik WHERE liczba=?";
        String liczbaSlownie="";
        try (PreparedStatement stmt = this.conn.prepareStatement(sql)) {
            stmt.setInt(1,liczba);

            try (ResultSet rs = stmt.executeQuery()){
                liczbaSlownie = rs.getString("slownie_"+jezyk.toString());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liczbaSlownie;
    }

    public String pobierzFormeGramatyczna (Jezyk jezyk, int poziom, int index){
        String sql = "SELECT * FROM formy WHERE poziom=?";
        String formaGramatyczna="";
        try (PreparedStatement stmt = this.conn.prepareStatement(sql)) {
            stmt.setInt(1,poziom);

            try (ResultSet rs = stmt.executeQuery()){
                formaGramatyczna = rs.getString("index"+index+"_"+jezyk.toString());
            }

        } catch (SQLException e){
            e.printStackTrace();
        }
        return formaGramatyczna;
    }

    public String pobierzNazweWaluty (Jezyk jezyk, String kodWaluty, int index){
        String sql = "SELECT * FROM waluty WHERE waluta=?";
        String nazwaWaluty="";

        try (PreparedStatement stmt = this.conn.prepareStatement(sql)){
            stmt.setString(1,kodWaluty);
            try (ResultSet rs = stmt.executeQuery()) {
                nazwaWaluty = rs.getString("index"+index+"_"+jezyk.toString());
            }

        } catch (SQLException e){
            e.printStackTrace();
        }
        return nazwaWaluty;

    }

}
