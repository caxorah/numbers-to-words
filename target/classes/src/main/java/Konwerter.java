import java.math.BigDecimal;

public class Konwerter {

    private Baza baza;
    Jezyk jezyk;

    public Konwerter(Jezyk jezyk){
        this.baza = new Baza();
        this.jezyk = jezyk;
    }

    public String kwotaSlownie(BigDecimal kwota, Waluta waluta){
        int czescCalkowita = splitBigDecimal(kwota)[0];
        int czescUlamkowa = splitBigDecimal(kwota)[1];

        String slownieCalkowite = liczbaCalkowitaSlownie(czescCalkowita)+" "
                +baza.pobierzNazweWaluty(jezyk,waluta.toString(),indexFormyGramatycznej(0,czescCalkowita));
        String slownieUlamki = liczbaCalkowitaSlownie(czescUlamkowa)+" "
                +baza.pobierzNazweWaluty(jezyk,waluta.toString()+"sub",indexFormyGramatycznej(0,czescUlamkowa));

        return slownieCalkowite+" "+slownieUlamki;
    }

    public String liczbaCalkowitaSlownie(int liczbaCalkowita){
        int idFormy = -1;
        String liczbaCalkowitaSlownie="";
        if (liczbaCalkowita==0){
            liczbaCalkowitaSlownie = "zero";
        } else {
            while (liczbaCalkowita>0){
                idFormy++;
                int kwotaCzastkowa = liczbaCalkowita%1000;
                liczbaCalkowitaSlownie = (liczbaCzastkowaSlownie(kwotaCzastkowa)+" "
                        +formaGramatyczna(idFormy, kwotaCzastkowa)).trim()+" "+liczbaCalkowitaSlownie.trim();
                liczbaCalkowita = (liczbaCalkowita-kwotaCzastkowa)/1000;
            }
        }
        return liczbaCalkowitaSlownie.trim();
    }

    private String liczbaCzastkowaSlownie (int liczbaCzastkowa){
        //liczba calkowita w zakresie 0-999
        int dziesiatki = liczbaCzastkowa%100;
        int setki = liczbaCzastkowa-dziesiatki;

        String cz1 = baza.pobierzLiczbeSlownie(jezyk,setki);
        String cz2;

        if (dziesiatki<21){
            cz2 = baza.pobierzLiczbeSlownie(jezyk,dziesiatki);
        } else {
            int jednosci = dziesiatki%10;
            int dziesiatkiZaokr = dziesiatki-jednosci;

            cz2 = (baza.pobierzLiczbeSlownie(jezyk,dziesiatkiZaokr).trim()+" "+
                    baza.pobierzLiczbeSlownie(jezyk,jednosci).trim());
        }
        return (cz1+" "+cz2).trim();
    }

    private String formaGramatyczna(int poziom, int liczbaCzesciowa){
        int indexFormy = indexFormyGramatycznej(poziom, liczbaCzesciowa);
        return baza.pobierzFormeGramatyczna(jezyk,poziom,indexFormy);
    }

    private int indexFormyGramatycznej (int poziom, int liczbaCzesciowa){
        if (poziom>0 && liczbaCzesciowa==0) {
            return  3;     //pusty string
        } else if (liczbaCzesciowa==1) {
            return  0;
        } else if (liczbaCzesciowa>10 && liczbaCzesciowa<15){
            return  2;
        } else if (liczbaCzesciowa%10>1 && liczbaCzesciowa%10<5) {
            return  1;
        } else {
            return  2;
        }
    }

    private int[] splitBigDecimal (BigDecimal bigdecimal){
        BigDecimal bdCalkowite = bigdecimal.divideToIntegralValue(BigDecimal.ONE);
        BigDecimal bdUlamkowe = bigdecimal.subtract(bdCalkowite).multiply(BigDecimal.valueOf(100));

        int[] tablicaWynikowa = new int[2];
        tablicaWynikowa[0]=bdCalkowite.intValue();
        tablicaWynikowa[1]=bdUlamkowe.intValue();
        return tablicaWynikowa;
    }

}

enum Jezyk {
    PL,
    EN
}

enum Waluta {
    PLN,
    USD,
    EUR,
    GBP,
    RUB
}
