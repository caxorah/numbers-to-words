import org.junit.Assert;
import org.junit.Test;
import java.math.BigDecimal;

public class KonwerterTest {

    @Test
    public void kwotaSlowniePL (){
        Konwerter konwerter = new Konwerter(Jezyk.PL);
        Assert.assertEquals("dwieście szesnaście złotych dwadzieścia groszy",
                konwerter.kwotaSlownie(BigDecimal.valueOf(216.20),Waluta.PLN));
        Assert.assertEquals("dwieście szesnaście dolarów dwadzieścia dwa centy",
                konwerter.kwotaSlownie(BigDecimal.valueOf(216.22),Waluta.USD));
        Assert.assertEquals("jeden tysiąc osiemset jeden funtów sześćdziesiąt trzy pensy",
                konwerter.kwotaSlownie(BigDecimal.valueOf(1801.63),Waluta.GBP));
        Assert.assertEquals("pięćset piętnaście rubli siedemnaście kopiejek",
                konwerter.kwotaSlownie(BigDecimal.valueOf(515.17),Waluta.RUB));
    }

    @Test
    public void kwotaSlownieEN (){
        Konwerter konwerter = new Konwerter(Jezyk.EN);
        Assert.assertEquals("zero euro fourteen cents",
                konwerter.kwotaSlownie(BigDecimal.valueOf(0.14),Waluta.EUR));
        Assert.assertEquals("five thousand three hundred dollars zero cents",
                konwerter.kwotaSlownie(BigDecimal.valueOf(5300.00),Waluta.USD));
    }

    @Test
    public void liczbaCalkowitaSlowniePL (){
        Konwerter konwerter = new Konwerter(Jezyk.PL);
        Assert.assertEquals("jeden milion dwadzieścia dwa",konwerter.liczbaCalkowitaSlownie(1000022));
        Assert.assertEquals("zero",konwerter.liczbaCalkowitaSlownie(0));
        Assert.assertEquals("czternaście",konwerter.liczbaCalkowitaSlownie(14));
        Assert.assertEquals("jeden milion czterysta",konwerter.liczbaCalkowitaSlownie(1000400));
        Assert.assertEquals("dwieście czterdzieści osiem",konwerter.liczbaCalkowitaSlownie(248));
    }

    @Test
    public void liczbaCalkowitaSlownieEN (){
        Konwerter konwerter = new Konwerter(Jezyk.EN);
        Assert.assertEquals("one million twenty two",konwerter.liczbaCalkowitaSlownie(1000022));
        Assert.assertEquals("zero",konwerter.liczbaCalkowitaSlownie(0));
        Assert.assertEquals("fourteen",konwerter.liczbaCalkowitaSlownie(14));
        Assert.assertEquals("one million four hundred",konwerter.liczbaCalkowitaSlownie(1000400));
        Assert.assertEquals("two hundred forty eight",konwerter.liczbaCalkowitaSlownie(248));
        Assert.assertEquals("three thousand six hundred twelve",konwerter.liczbaCalkowitaSlownie(3612));
    }
}